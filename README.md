# Mozipenztar

A félév során megvalósítandó project munkánk egy mozipénztár elkészítése lesz. A következő role-okkal és funkciókkal:
Admin, péntáros, vásárló

Vásárló tud filmlistát nézni, vetítés listát nézni, vetítésekre székeket foglalni, saját foglalását törölni. Pénztáros ezen felül tud új vetítéseket kiírni, illetve jegyeket "eladni" (akkor már nem lehet törölni foglalást), vetítéseket törölni ha üres a terem. Admin mindezen felül tud új filmet felvenni, meg megnézni a regisztrált felhasználók listáját.