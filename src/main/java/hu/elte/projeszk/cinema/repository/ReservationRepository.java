package hu.elte.projeszk.cinema.repository;

import hu.elte.projeszk.cinema.model.Reservation;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ReservationRepository extends CrudRepository<Reservation, Long> {
    Optional<Reservation> findById(Long id);

    @Override
    Iterable<Reservation> findAll();

    Iterable<Reservation> findAllByShowingId(Long id);

    @Override
    Reservation save(Reservation reservation);
}
