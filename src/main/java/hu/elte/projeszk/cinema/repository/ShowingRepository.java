package hu.elte.projeszk.cinema.repository;

import hu.elte.projeszk.cinema.model.Showing;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ShowingRepository extends CrudRepository<Showing, Long> 
{
    Optional<Showing> findById(Long id);

    @Override
    Iterable<Showing> findAll();

    @Override
    Showing save(Showing showing);

    void deleteById(Long id);
}
