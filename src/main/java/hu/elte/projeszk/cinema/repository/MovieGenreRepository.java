package hu.elte.projeszk.cinema.repository;

import hu.elte.projeszk.cinema.model.MovieGenre;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MovieGenreRepository extends CrudRepository<MovieGenre, Long> 
{
    Optional<MovieGenre> findById(Long id);

    Iterable<MovieGenre> findAllByMovieId(Long id);

    void deleteAllByMovieId(Long id);

    @Override
    MovieGenre save(MovieGenre movieGenre);
}
