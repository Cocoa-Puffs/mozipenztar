package hu.elte.projeszk.cinema.repository;

import hu.elte.projeszk.cinema.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
    Optional<User> findByUsername(String username);

    Optional<User> findByUsernameAndPassword(String username, String password);

    Optional<User> findById(Long id);

    @Override
    Iterable<User> findAll();

    @Override
    User save(User user);
}
