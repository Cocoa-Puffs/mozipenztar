package hu.elte.projeszk.cinema.repository;

import hu.elte.projeszk.cinema.model.Movie;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MovieRepository extends CrudRepository<Movie, Long> 
{
    Optional<Movie> findById(Long id);

    @Override
    Iterable<Movie> findAll();

    @Override
    Movie save(Movie movie);

    void deleteById(Long id);
}
