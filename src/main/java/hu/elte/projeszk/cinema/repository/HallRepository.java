package hu.elte.projeszk.cinema.repository;

import hu.elte.projeszk.cinema.model.Hall;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface HallRepository extends CrudRepository<Hall, Long> 
{
    Optional<Hall> findById(Long id);

    @Override
    Iterable<Hall> findAll();

    @Override
    Hall save(Hall hall);
}
