package hu.elte.projeszk.cinema.repository;

import hu.elte.projeszk.cinema.model.Seat;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SeatRepository extends CrudRepository<Seat, Long> 
{
    Optional<Seat> findById(Long id);

    @Override
    Iterable<Seat> findAll();

    Iterable<Seat> findAllByReservationId(Long id);

    @Override
    Seat save(Seat seat);
}
