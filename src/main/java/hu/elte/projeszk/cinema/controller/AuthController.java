package hu.elte.projeszk.cinema.controller;

import hu.elte.projeszk.cinema.model.User;
import hu.elte.projeszk.cinema.service.UserService;
import hu.elte.projeszk.cinema.service.annotations.Role;
import hu.elte.projeszk.cinema.service.exceptions.UserNotValidException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static hu.elte.projeszk.cinema.model.User.Role.ADMIN;
import static hu.elte.projeszk.cinema.model.User.Role.CASHIER;
import static hu.elte.projeszk.cinema.model.User.Role.USER;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class AuthController {

    @Autowired
    private UserService userService;

    @PostMapping("/login")
    public ResponseEntity login(@RequestBody User user) {
        try {
            User u = userService.login(user);
            return ResponseEntity.ok(new Object(){
                public String username = u.getUsername();
                public User.Role role = u.getRole();
            });
        } catch (UserNotValidException ex) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
    }

    @Role({USER, CASHIER, ADMIN})
    @PostMapping("/logout")
    public ResponseEntity logout(){
        userService.logout();
        return ResponseEntity.ok().build();
    }

    @PostMapping("/register")
    public ResponseEntity register(@RequestBody User user) {
        try {
            userService.register(user);
            return ResponseEntity.ok().build();
        } catch(UserNotValidException ex) {
            return ResponseEntity.badRequest().build();
        }
    }
}
