package hu.elte.projeszk.cinema.controller;

import hu.elte.projeszk.cinema.model.Showing;
import hu.elte.projeszk.cinema.repository.HallRepository;
import hu.elte.projeszk.cinema.service.ShowingService;
import hu.elte.projeszk.cinema.service.annotations.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.sql.Time;
import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.List;

import static hu.elte.projeszk.cinema.model.User.Role.ADMIN;

@CrossOrigin
@RestController
@RequestMapping("/api/showings")
public class ShowingController {

    @Autowired
    private ShowingService showingService;

    @Autowired
    private HallRepository hallRepository;

    @GetMapping
    public ResponseEntity listShowings(){
        Iterable<Showing> showings = showingService.list();
        List<Object> objects = new ArrayList<>();
        for(Showing s : showings) {
            objects.add(new Object(){
                public Long id = s.getId();
                public DayOfWeek day = s.getDay();
                public Time time = s.getTime();
                public String title = s.getMovie().getTitle();
                public short length = s.getMovie().getLength();
            });
        }

        return ResponseEntity.ok(objects);
    }

    @Role(ADMIN)
    @PostMapping("/add")
    public ResponseEntity add(@RequestBody Showing showing) {
        showingService.create(showing);
        return ResponseEntity.ok().build();
    }

    @Role(ADMIN)
    @DeleteMapping("/{id}")
    @Transactional
    public ResponseEntity delete(@PathVariable Long id) {
        showingService.delete(id);
        return ResponseEntity.ok().build();
    }

    @Role(ADMIN)
    @GetMapping("/halls")
    public ResponseEntity listHalls() {
        return ResponseEntity.ok(hallRepository.findAll());
    }
}
