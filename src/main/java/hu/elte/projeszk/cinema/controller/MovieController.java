package hu.elte.projeszk.cinema.controller;

import hu.elte.projeszk.cinema.model.Movie;
import hu.elte.projeszk.cinema.model.MovieGenre;
import hu.elte.projeszk.cinema.repository.MovieGenreRepository;
import hu.elte.projeszk.cinema.service.MovieService;
import hu.elte.projeszk.cinema.service.annotations.Role;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

import static hu.elte.projeszk.cinema.model.User.Role.ADMIN;

@CrossOrigin
@RestController
@RequestMapping("/api/movies")
public class MovieController 
{
    @Autowired
    private MovieService movieService;

    @Autowired
    private MovieGenreRepository movieGenreRepository;

    @GetMapping
    public ResponseEntity<Iterable<Movie>> listMovies()
	{
        return ResponseEntity.ok(movieService.list());
    }

    @GetMapping("/{id}")
    public ResponseEntity getMovie(@PathVariable Long id)
	{
        try 
		{
            Iterable<MovieGenre> movieGenresAll = movieGenreRepository.findAllByMovieId(id);

            List<MovieGenre.Genre> movieGenresList = new ArrayList<>();
            movieGenresAll.forEach(genre -> movieGenresList.add(genre.getGenre()));

            return ResponseEntity.ok(new Object()
			{
                public Movie data = movieService.getMovieById(id);
                public List<MovieGenre.Genre> movieGenres = movieGenresList;
            });
        } 
		catch(IllegalArgumentException ex) 
		{
            return ResponseEntity.badRequest().build();
        }
    }

    @Role(ADMIN)
    @PostMapping("/add")
    public ResponseEntity add(@RequestBody Iterable<MovieGenre> movieGenres) 
	{
        movieService.create(((List<MovieGenre>)movieGenres).get(0).getMovie(), movieGenres);		
        return ResponseEntity.ok().build();
    }

    @Role(ADMIN)
    @PatchMapping("/{id}")
    @Transactional
    public ResponseEntity editMovie(@PathVariable Long id, @RequestBody Iterable<MovieGenre> movieGenres) 
	{
        try 
		{
            movieService.update(id, ((List<MovieGenre>)movieGenres).get(0).getMovie(), movieGenres);		
            return ResponseEntity.ok().build();
        } 
		catch(IllegalArgumentException ex) 
		{
            return ResponseEntity.badRequest().build();
        }
    }

    @Role(ADMIN)
    @DeleteMapping("/{id}")
    @Transactional
    public ResponseEntity deleteMovie(@PathVariable Long id) 
	{
        movieService.delete(id);
        return ResponseEntity.ok().build();
    }
}
