package hu.elte.projeszk.cinema.controller;

import hu.elte.projeszk.cinema.model.User;
import hu.elte.projeszk.cinema.service.UserService;
import hu.elte.projeszk.cinema.service.annotations.Role;
import hu.elte.projeszk.cinema.service.exceptions.UserNotValidException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import static hu.elte.projeszk.cinema.model.User.Role.*;

@Component
@CrossOrigin
@RestController
@RequestMapping("/api/users")
public class UserController {

    @Autowired
    private UserService userService;

    @Role({ADMIN})
    @GetMapping
    public ResponseEntity listUsers(Model model){
        return ResponseEntity.ok(userService.list());
    }

    @Role({ADMIN, USER})
    @GetMapping("/edit")
    public ResponseEntity edit() {
        User user = userService.getUser();
        user.setPassword("");
        return ResponseEntity.ok(user);
    }

    @PostMapping("/register")
    public ResponseEntity register(@RequestBody User user) {
        try {
            return ResponseEntity.ok(userService.register(user));
        } catch (UserNotValidException e) {
            return ResponseEntity.badRequest().build();
        }
    }

    @PostMapping("/login")
    public ResponseEntity login(@RequestBody User user) {
        try {
            return ResponseEntity.ok(userService.login(user));
        } catch (UserNotValidException ex) {
            return ResponseEntity.status(404).build();
        }
    }

    @Role({ADMIN, CASHIER, USER})
    @PostMapping("/logout")
    public ResponseEntity logout() {
        userService.logout();
        return ResponseEntity.ok().build();
    }

    @Role({ADMIN, USER})
    @PatchMapping("/edit")
    public ResponseEntity edit(@RequestBody User user) {
        try {
            user.setRole(userService.getUser().getRole());
            userService.update(userService.getUser().getId(), user);
            return ResponseEntity.ok().build();
        } catch(IllegalArgumentException | UserNotValidException ex) {
            return ResponseEntity.badRequest().build();
        }
    }

    @Role(ADMIN)
    @GetMapping("/edit/{id}")
    public ResponseEntity editUser(@PathVariable Long id) {
        try {
            return ResponseEntity.ok(userService.getUserById(id));
        } catch(IllegalArgumentException ex) {
            return ResponseEntity.badRequest().build();
        }
    }

    @Role(ADMIN)
    @PatchMapping("/edit/{id}")
    public ResponseEntity editUser(@PathVariable Long id, @RequestBody User user) {
        try {
            userService.update(id, user);
            return ResponseEntity.ok().build();
        } catch(IllegalArgumentException | UserNotValidException ex) {
            return ResponseEntity.badRequest().build();
        }
    }
}
