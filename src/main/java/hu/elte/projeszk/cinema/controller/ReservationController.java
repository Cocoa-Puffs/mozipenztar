package hu.elte.projeszk.cinema.controller;

import hu.elte.projeszk.cinema.model.Hall;
import hu.elte.projeszk.cinema.model.Reservation;
import hu.elte.projeszk.cinema.model.Seat;
import hu.elte.projeszk.cinema.repository.SeatRepository;
import hu.elte.projeszk.cinema.service.ReservationService;
import hu.elte.projeszk.cinema.service.ShowingService;
import hu.elte.projeszk.cinema.service.annotations.Role;
import hu.elte.projeszk.cinema.service.exceptions.ReservationNotValidException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Time;
import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.List;

import static hu.elte.projeszk.cinema.model.User.Role.ADMIN;
import static hu.elte.projeszk.cinema.model.User.Role.CASHIER;
import static hu.elte.projeszk.cinema.model.User.Role.USER;

@CrossOrigin
@RestController
@RequestMapping("/api/reservations")
public class ReservationController {
    @Autowired
    private ReservationService reservationService;

    @Autowired
    private SeatRepository seatRepository;

    @Autowired
    private ShowingService showingService;

    @Role({ADMIN, CASHIER})
    @GetMapping
    public ResponseEntity listReservation(){
        Iterable<Reservation> reservations = reservationService.list();
        List<Object> objects = new ArrayList<>();
        for(Reservation r : reservations) {
            objects.add(new Object() {
                public Long id = r.getId();
                public Reservation.Status status = r.getStatus();
                public String name = r.getUser().getName();
                public DayOfWeek day = r.getShowing().getDay();
                public Time time = r.getShowing().getTime();
                public String title = r.getShowing().getMovie().getTitle();
            });
        }

        return ResponseEntity.ok(objects);
    }

    @Role(CASHIER)
    @PatchMapping("/sell/{id}")
    public ResponseEntity sell(@PathVariable Long id) {
        try {
            reservationService.update(id, Reservation.Status.SOLD);
            return ResponseEntity.ok().build();
        } catch(IllegalArgumentException ex) {
            return ResponseEntity.badRequest().build();
        }
    }

    @Role({ADMIN, CASHIER, USER})
    @GetMapping("/get/{id}")
    public ResponseEntity getReservations(@PathVariable Long id){
        Iterable<Reservation> reservations = reservationService.listByShowingId(id);

        Hall hall;
        try {
            hall = showingService.getShowingById(id).getHall();
        } catch(IllegalArgumentException ex) {
            return ResponseEntity.badRequest().build();
        }

        List<Object> objects = new ArrayList<>();
        for(Reservation r : reservations) {
            Iterable<Seat> se = seatRepository.findAllByReservationId(r.getId());
            List<Object> seatData = new ArrayList<>();
            for(Seat s : se) {
                seatData.add(new Object(){
                    public Byte row = s.getRow();
                    public Byte column = s.getColumn();
                });
            }

            objects.add(new Object() {
                public Object reservation = new Object(){
                    public Long id = r.getId();
                    public Reservation.Status status = r.getStatus();

                };
                public List<Object> seats = seatData;
            });
        }

        return ResponseEntity.ok(new Object() {
            public int rows = hall.getRows();
            public int columns = hall.getColumns();
            public List<Object> reservations = objects;
        });
    }

    @Role(CASHIER)
    @PostMapping("/sell")
    public ResponseEntity sell(@RequestBody Iterable<Seat> seats){
        try {
            reservationService.add(((List<Seat>)seats).get(0).getReservation(), seats, Reservation.Status.SOLD);
            return ResponseEntity.ok().build();
        } catch(ReservationNotValidException ex) {
            return ResponseEntity.badRequest().build();
        }
    }

    @Role(USER)
    @PostMapping("/new")
    public ResponseEntity add(@RequestBody Iterable<Seat> seats){
        try {
            reservationService.add(((List<Seat>)seats).get(0).getReservation(), seats, Reservation.Status.RESERVED);
            return ResponseEntity.ok().build();
        } catch(ReservationNotValidException ex) {
            return ResponseEntity.badRequest().build();
        }
    }
}
