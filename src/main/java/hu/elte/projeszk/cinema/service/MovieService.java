package hu.elte.projeszk.cinema.service;

import hu.elte.projeszk.cinema.model.Movie;
import hu.elte.projeszk.cinema.model.MovieGenre;
import hu.elte.projeszk.cinema.repository.MovieGenreRepository;
import hu.elte.projeszk.cinema.repository.MovieRepository;
import hu.elte.projeszk.cinema.repository.ShowingRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class MovieService 
{
    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private MovieGenreRepository movieGenreRepository;

    @Autowired
    private ShowingRepository showingRepository;

    public Iterable<Movie> list() 
	{
        Iterable<Movie> movies = movieRepository.findAll();
        movies.forEach(movie -> movie.setSynopsis(null));
		
        return movies;
    }

    public Movie create(Movie movie, Iterable<MovieGenre> movieGenres) 
	{
        Movie savedMovie = movieRepository.save(movie);
        movieGenres.forEach(genre -> genre.setMovie(savedMovie));
        movieGenres.forEach(genre -> movieGenreRepository.save(genre));
		
        return savedMovie;
    }

    public Movie update(Long id, Movie movie, Iterable<MovieGenre> movieGenres) 
	{
        Optional<Movie> selectedMovie = movieRepository.findById(id);
		
        if (selectedMovie.isPresent()) 
		{
            movieGenreRepository.deleteAllByMovieId(id);
            movie.setId(selectedMovie.get().getId());
            Movie updatedMovie = movieRepository.save(movie);
            movieGenres.forEach(genre -> genre.setMovie(updatedMovie));
            movieGenres.forEach(movieGenre -> movieGenreRepository.save(movieGenre));
			
            return updatedMovie;
        }
		
        throw new IllegalArgumentException();
    }

    public void delete(Long id) 
	{
        movieGenreRepository.deleteAllByMovieId(id);
        movieRepository.deleteById(id);
    }

    public Movie getMovieById(Long id) 
	{
        Optional<Movie> selectedMovie = movieRepository.findById(id);
		
        if (selectedMovie.isPresent()) 
		{
            return selectedMovie.get();
        }
		
        throw new IllegalArgumentException();
    }
}
