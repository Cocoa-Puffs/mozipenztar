package hu.elte.projeszk.cinema.service;

import hu.elte.projeszk.cinema.model.Showing;
import hu.elte.projeszk.cinema.repository.ShowingRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ShowingService 
{
    @Autowired
    private ShowingRepository showingRepository;

    public Iterable<Showing> list() 
	{
        return showingRepository.findAll();
    }

    public Showing create(Showing showing)
	{
        return showingRepository.save(showing);
    }

    public void delete(Long id)
	{
        showingRepository.deleteById(id);
    }

    public Showing getShowingById(Long id)
	{
        Optional<Showing> selectedShowing = showingRepository.findById(id);
		
        if (selectedShowing.isPresent())
		{
            return selectedShowing.get();
        }
		
        throw new IllegalArgumentException();
    }
}