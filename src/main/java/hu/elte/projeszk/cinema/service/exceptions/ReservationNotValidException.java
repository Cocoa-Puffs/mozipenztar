package hu.elte.projeszk.cinema.service.exceptions;

public class ReservationNotValidException extends Exception {

    public ReservationNotValidException() {
        super("Invalid reservation!");
    }
}
