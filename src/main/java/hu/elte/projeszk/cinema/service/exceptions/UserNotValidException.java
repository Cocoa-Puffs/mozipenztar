package hu.elte.projeszk.cinema.service.exceptions;

public class UserNotValidException extends Exception {

    public UserNotValidException() {
        super("Invalid user!");
    }
}
