package hu.elte.projeszk.cinema.service;

import hu.elte.projeszk.cinema.model.Reservation;
import hu.elte.projeszk.cinema.model.Seat;
import hu.elte.projeszk.cinema.repository.ReservationRepository;
import hu.elte.projeszk.cinema.repository.SeatRepository;
import hu.elte.projeszk.cinema.service.exceptions.ReservationNotValidException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ReservationService {

    @Autowired
    private ReservationRepository reservationRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private SeatRepository seatRepository;

    public Iterable<Reservation> list() {
        return reservationRepository.findAll();
    }

    public Reservation add(Reservation reservation, Iterable<Seat> seats, Reservation.Status status) throws ReservationNotValidException {
        if(areReservationSeatsValid(reservation, seats)) {
            reservation.setUser(userService.getUser());
            reservation.setStatus(status);
            Reservation savedReservation = reservationRepository.save(reservation);
            seats.forEach(seat -> seat.setReservation(savedReservation));
            seats.forEach(seat -> seatRepository.save(seat));
            return savedReservation;
        }
        throw new ReservationNotValidException();
    }

    public Reservation update(Long id, Reservation.Status status) {
        Optional<Reservation> selectedReservation = reservationRepository.findById(id);
        if(selectedReservation.isPresent()) {
            Reservation reservation = selectedReservation.get();
            reservation.setStatus(status);
            return reservationRepository.save(reservation);
        }
        throw new IllegalArgumentException();
    }

    public Iterable<Reservation> listByShowingId(Long id) {
        Iterable<Reservation> reservations = reservationRepository.findAllByShowingId(id);
        reservations.forEach(r -> r.setUser(null));
        return reservations;
    }

    private boolean areReservationSeatsValid(Reservation reservation, Iterable<Seat> seats) {
        List<Reservation> bookings = (List<Reservation>) reservationRepository.findAllByShowingId(reservation.getShowing().getId());
        List<Seat> bookedSeats = new ArrayList<>();
        for(Reservation r : bookings){
            bookedSeats.addAll((List)seatRepository.findAllByReservationId(r.getId()));
        }

        for(Seat s : seats) {
            for(Seat bs : bookedSeats) {
                if(s.getColumn() == bs.getColumn() && s.getRow() == bs.getRow()) {
                    return false;
                }
            }
        }

        return true;
    }
}
