package hu.elte.projeszk.cinema.service;

import hu.elte.projeszk.cinema.model.User;
import hu.elte.projeszk.cinema.repository.UserRepository;
import hu.elte.projeszk.cinema.service.exceptions.UserNotValidException;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;

@Service
@Data
public class UserService {

    @Autowired
    private UserRepository userRepository;

    private User user;

    public Iterable<User> list(){
        Iterable<User> users = userRepository.findAll();
        for(User u : users) {
            u.setPassword("");
        }
        return users;
    }

    public User login(User user) throws UserNotValidException {
        Optional<User> optionalUser = isLoginSuccessful(user);
        if (optionalUser.isPresent()) {
            this.user = optionalUser.get();
            return this.user;
        }
        throw new UserNotValidException();
    }

    public void logout() {
        this.user = null;
    }

    public User register(User user) throws UserNotValidException {
        if(isValidUser(user)&&!userRepository.findByUsername(user.getUsername()).isPresent()) {
            user.setRole(User.Role.USER);
            this.user = userRepository.save(user);
            return user;
        }
        throw new UserNotValidException();
    }

    public User update(Long id, User user) throws UserNotValidException {
        if(!isValidUser(user)||!isUserPresent(id)) {
            throw new UserNotValidException();
        }
        user.setId(id);
        return this.user = userRepository.save(user);
    }

    public User getUserById(Long id) {
        Optional<User> selectedUser = userRepository.findById(id);
        if(selectedUser.isPresent()) {
            User user = selectedUser.get();
            user.setPassword(null);
            return user;
        }
        throw new IllegalArgumentException();
    }

    private boolean isUserPresent(Long id) {
        try{
            getUserById(id);
            return true;
        }catch(IllegalArgumentException ex){
            return false;
        }
    }

    public Optional<User> isLoginSuccessful(User user) {
        return userRepository.findByUsernameAndPassword(user.getUsername(), user.getPassword());
    }

    public boolean isLoggedIn() {
        return Objects.nonNull(user);
    }

    private boolean isValidUser(User user) {
        return (user.getUsername().length() < 20 && user.getName().length() < 70 && user.getPhone().length() < 30);
    }
}
