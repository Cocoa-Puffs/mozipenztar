package hu.elte.projeszk.cinema.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import java.sql.Date;

@Entity
@Table(name = "MOVIES")
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Movie extends BaseEntity 
{
    @Column(nullable = false, unique = true, length = 100)
    private String title;

    @Column(nullable = false, length = 70)
    private String director;

    @Column(nullable = false, length = 300)
    private String synopsis;

    @Column(nullable = false)
    private short length;

    @Column(nullable = false, length = 150)
    private String posterPath;

    @Column(nullable = false)
    private Date releaseDate;
}
