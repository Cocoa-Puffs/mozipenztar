package hu.elte.projeszk.cinema.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "RESERVATIONS")
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Reservation extends BaseEntity {

    @JoinColumn(nullable = false)
    @ManyToOne(targetEntity = User.class)
    private User user;

    @JoinColumn(nullable = false)
    @ManyToOne(targetEntity = Showing.class)
    private Showing showing;

    @Column(nullable = false, length = 15)
    @Enumerated(EnumType.STRING)
    private Status status;

    public enum Status {
        RESERVED, SOLD
    }
}
