package hu.elte.projeszk.cinema.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "MOVIE_GENRES")
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class MovieGenre extends BaseEntity 
{
    @JoinColumn(nullable = false)
    @ManyToOne(targetEntity = Movie.class)
    private Movie movie;

    @Column(nullable = false, length = 20)
    @Enumerated(EnumType.STRING)
    private Genre genre;

    public enum Genre 
	{
        ACTION, HORROR
    }
}
