package hu.elte.projeszk.cinema.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "HALLS")
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Hall extends BaseEntity 
{
    @Column(nullable = false, unique = true, length = 50)
    private String name;

    @Column(nullable = false)
    private byte rows;

    @Column(nullable = false)
    private byte columns;
}
