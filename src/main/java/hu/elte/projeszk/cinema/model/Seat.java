package hu.elte.projeszk.cinema.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "SEATS")
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Seat extends BaseEntity 
{
    @JoinColumn(nullable = false)
    @ManyToOne(targetEntity = Reservation.class)
    private Reservation reservation;

    @Column(nullable = false)
    private byte row;

    @Column(nullable = false)
    private byte column;
}
