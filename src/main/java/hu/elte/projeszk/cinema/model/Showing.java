package hu.elte.projeszk.cinema.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import java.sql.Time;
import java.time.DayOfWeek;

@Entity
@Table(name = "SHOWINGS")
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Showing extends BaseEntity 
{
    @JoinColumn(nullable = false)
    @ManyToOne(targetEntity = Movie.class)
    private Movie movie;

    @JoinColumn(nullable = false)
    @ManyToOne(targetEntity = Hall.class)
    private Hall hall;

    @Column(nullable = false)
    @Enumerated(EnumType.ORDINAL)
    private DayOfWeek day;

    @Column(nullable = false)
    private Time time;
}
